﻿using FluentAssertions;
using NUnit.Framework;

namespace FizzBuzz.Tests
{
    [TestFixture]
    public class ProgramTests
    {
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(4)]
        [TestCase(11)]
        public void FizzBuzz_NumberIsNotDivisibleBy3Or5_ReturnsNumber(int number)
        {
            Program.FizzBuzz(number).Should().Be(number.ToString());
        }

        [TestCase(3)]
        [TestCase(6)]
        [TestCase(12)]
        public void FizzBuzz_NumberIsDivisibleBy3ButNotBy5_ReturnsFizz(int number)
        {
            Program.FizzBuzz(number).Should().Be("Fizz");
        }

        [TestCase(5)]
        [TestCase(10)]
        [TestCase(20)]
        public void FizzBuzz_NumberIsDivisibleBy5ButNotBy3_ReturnsBuzz(int number)
        {
            Program.FizzBuzz(number).Should().Be("Buzz");
        }

        [TestCase(15)]
        [TestCase(30)]
        public void FizzBuzz_NumberIsDivisibleByBoth3And5_ReturnsFizzBuzz(int number)
        {
            Program.FizzBuzz(number).Should().Be("FizzBuzz");
        }
    }
}
