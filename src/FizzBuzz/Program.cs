﻿using System;

namespace FizzBuzz
{
    public class Program
    {
        public static void Main(string[] args)
        {
            for (var number = 1; number <= 30; number++)
            {
                Console.WriteLine(FizzBuzz(number));
            }
            Console.ReadLine();
        }

        public static string FizzBuzz(int number)
        {
            var divisibleBy3 = number % 3 == 0;
            var divisibleBy5 = number % 5 == 0;

            if (divisibleBy3 && divisibleBy5)
            {
                return "FizzBuzz";
            }
            else if (divisibleBy3)
            {
                return "Fizz";
            }
            else if (divisibleBy5)
            {
                return "Buzz";
            }
            else
            {
                return number.ToString();
            }
        }
    }
}
